FROM debian:9 as build

RUN apt update && apt install -y wget gcc make \
&& apt install -y libpcre++-dev \
&& apt install -y zlib1g \
&& apt install -y zlib1g-dev

ENV nginx=nginx-1.0.5

RUN wget http://nginx.org/download/$nginx.tar.gz \
&& tar xvfz $nginx.tar.gz \
&& cd $nginx \
&& ./configure \
&&  make \
&& make install

FROM debian:9

WORKDIR /usr/local/nginx/sbin

COPY --from=build /usr/local/nginx/sbin/nginx .

RUN mkdir ../logs ../conf && touch ../logs/error.log chmod +x nginx

COPY mime.types /usr/local/nginx/conf/mime.types

CMD ["./nginx", "-g", "daemon off;"]
